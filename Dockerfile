FROM python

WORKDIR /app

RUN apt install libpq-dev
RUN pip install psycopg2
RUN pip install pyTelegramBotAPI
RUN pip install docxtpl
RUN mkdir output

COPY . /app

CMD ["python", "main.py"]