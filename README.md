The bot helps to test software developers 
asking them questions, sending the answers to the hardcoded email addresses,
or uploading all documents with answers to telegram by command '/upload'
(secret phrase: 'pleeease')

Currently, the list of questions is hardcoded, 
to take questions from the database you should uncomment appropriate strings.

To build the bot image use the command in the project root folder:
```console
sudo docker build . -t wbot
```

To run a container from the image:
```console
sudo docker run -it -v ${PWD}/weigandt_test_bot/output:/output wbot
```