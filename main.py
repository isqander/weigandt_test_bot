import telebot
import psycopg2
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import encoders
from docxtpl import DocxTemplate
from os import walk
from datetime import date

tb = telebot.TeleBot('5145626522:AAHS4tuxC2fiMq1tfrtzhjUeBsr4tZHrFuo')

### Приложение может работать как от бд, так и с захардкоженными вопросами, для работы через бд раскомментируйте конфиг connection и метод load_questions ниже

# connection = psycopg2.connect(
#     database="postgres", user='postgres_admin@db-postgres-dev-uk-01', password='qKe-8bB-Ne3-iWL',
#     host='db-postgres-dev-uk-01.postgres.database.azure.com', port='5432'
# )
# cursor = connection.cursor()

question_number = 0


class Question:
    def __init__(self, name, question):
        self._name = name
        self._question = question

    def get_name(self):
        return self._name

    def get_question(self):
        return self._question

    def get_answer(self):
        return self._answer

    def set_answer(self, answer):
        self._answer = answer


questions = [Question('q1',
                      'Привет! Этот тест из 19 вопросов для предварительной оценки вашего уровня.\nНапишите пожалуйста своё имя и первую букву фамилии:'),
             Question('q2',
                      'Какие решения принимал по замене технологического стека (базы данных, очереди, оркестраторы и тд)?'),
             Question('q3', 'Назови одно техническое улучшение, которое сделал на проекте, помимо бизнес задач?'),
             Question('q4', 'Мотивация смены работы - почему?'),
             Question('q5', "Какую версию java используете в проде?"),
             Question('q6', "Какой GC (garbage collector/сборщик мусора) используете сейчас?"),
             Question('q7',
                      "Будет ли обработка сложения чисел от 1 до 100 быстрее, если использовать вместо stream parallel stream, почему?"),
             Question('q8', "Делал ли свой Spring boot стартер? Для чего?"),
             Question('q9', "Для чего нужны профайлы в Spring?"),
             Question('q10', "Как реализованы extension методы в kotlin?"),
             Question('q11', "С какими библиотеками реактивного программирования работал?"),
             Question('q12', "Flux и Mono по дефолту cold или hot паблишер?"),
             Question('q13', "Чем отличаются hot и cold паблишеры"),
             Question('q14', "Что будет, если в реактивном сервисе использовать блокирующий драйвер БД?"),
             Question('q15',
                      "Какие задачи по инфраструктуре(базы данных, очереди, оркестраторы и тд) делал сам руками?"),
             Question('q16', "Какие способы есть у распределенных баз данных масштабировать нагрузку?"),
             Question('q17', "Как обеспечивается сохранность информации при падении одной из нод базы данных?"),
             Question('q18', "Какую роль выполняет deployment в Kubernetes, за что отвечает service?"),
             Question('q19',
                      "Можно ли из топика (распределен по 3 партициям) прочитать сообщения в том же порядке, в котором они были записаны? Почему?"),
             Question('q20', "Как сделать так, чтобы все сообщения по одному клиенту попали в одну партицию?"),
             Question('q21',
                      "Если вы хотите внести какие-то коментарии к вопросам, чтото исправить/дополнить, то напишите это единым сообщением:"),
             Question('q22', "Насколько удобен подобный формат тестирования? Что бы вы исправили/улучшили?")
             ]


@tb.message_handler(commands=["startTest"])
def start_test(m):
    # load_questions()
    tb.send_message(m.chat.id, questions.__getitem__(question_number).get_question())
    tb.register_next_step_handler(m, get_answer)


@tb.message_handler(commands=["upload"])
def upload(m):
    tb.send_message(m.chat.id, 'Скажите волшебное слово)')
    tb.register_next_step_handler(m, upload2)


@tb.message_handler(content_types=['text'])
def start(m, res=False):
    tb.send_message(m.chat.id, 'Введите команду /startTest для начала тестирования')


@tb.message_handler(content_types=['text'])
def get_answer(m):
    global question_number
    questions.__getitem__(question_number).set_answer(m.text)
    question_number += 1
    if question_number >= questions.__len__():
        save_result()
        tb.send_message(m.chat.id, 'Спасибо! Ваш результат сохранён')
        tb.close()
    else:
        message = questions.__getitem__(question_number).get_question()
        if question_number > 0 and question_number < 20:
            message = "Вопрос " + str(question_number) + " из " + str(questions.__len__() - 3) + "\n" + message
        tb.send_message(m.chat.id, message)
        tb.register_next_step_handler(m, get_answer)


def save_result():
    doc = DocxTemplate("Screening.docx")
    context = {}
    for question in questions:
        context[question.get_name()] = question.get_answer()
    doc.render(context)
    filename = "Screening_" + (questions.__getitem__(0).get_answer() + str(date.today())).replace(" ", "_") + ".docx"
    filename_path = "output/" + filename
    doc.save(filename_path)

    part = MIMEBase('application', "octet-stream")
    part.set_payload(open(filename_path, "rb").read())
    encoders.encode_base64(part)
    part.add_header('Content-Disposition', 'attachment', filename=filename)

    emails_to = ["aleksandr.kiriushin@weigandt-consulting.com", "mariia.stolovaya@weigandt-consulting.com"]

    for email_to in emails_to:
        msg = MIMEMultipart()
        msg['From'] = "weigandtbot@gmail.com"
        msg['To'] = email_to
        msg['Subject'] = "Test Results"
        msg.attach(part)
        smtpObj = smtplib.SMTP('smtp.gmail.com', 587)
        smtpObj.starttls()
        smtpObj.login('weigandtbot@gmail.com', 'cvwmpvinlfeumsip')
        smtpObj.sendmail("weigandtbot@gmail.com", email_to, msg.as_string())
        smtpObj.quit()


@tb.message_handler(content_types=['text'])
def upload2(m):
    if m.text == 'pleeease':
        filenames = next(walk('output/'), (None, None, []))[2]
        for file in filenames:
            doc = open('output/' + file, 'rb')
            tb.send_document(m.chat.id, doc)


# def load_questions():
#     cursor.execute('select * from questions')
#     for row in cursor.fetchall():
#         questions.append(Question(row[1], row[2]))


tb.polling(none_stop=True, interval=0)
